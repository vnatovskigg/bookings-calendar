import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Alert as AlertComponent, AlertTitle } from '@material-ui/lab';

const useStyles = makeStyles((theme) => ({
	root: {
		width: '100%',
		'& > * + *': {
			marginTop: theme.spacing(2),
		},
	},
}));

export default function Alert({ type, title, text }) {
	const classes = useStyles();

	return (
		<div className={classes.root}>
			<AlertComponent severity={type}>
				<AlertTitle>{title}</AlertTitle>
				{text}
			</AlertComponent>
		</div>
	);
}
