import { useState } from 'react';
import { Modal as MaterialUiModal } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import BookingForm from '../BookingForm/BookingForm';

function rand() {
	return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
	const top = 50 + rand();
	const left = 50 + rand();

	return {
		top: `${top}%`,
		left: `${left}%`,
		transform: `translate(-${top}%, -${left}%)`,
	};
}

const useStyles = makeStyles((theme) => ({
	paper: {
		position: 'absolute',
		width: 400,
		backgroundColor: theme.palette.background.paper,
		border: '2px solid #000',
		boxShadow: theme.shadows[5],
		padding: theme.spacing(2, 4, 3),
	},
}));

function Modal({ openModal, setOpenModal, calendar, setCalendar }) {
	const [modalStyle] = useState(getModalStyle);

	const classes = useStyles();

	const handleClose = () => {
		setOpenModal(false);
	};

	const body = (
		<div style={modalStyle} className={classes.paper}>
			<BookingForm calendar={calendar} setCalendar={setCalendar} setOpenModal={setOpenModal} />
			<Modal />
		</div>
	);

	return (
		<div className='modal'>
			<MaterialUiModal open={openModal} onClose={handleClose} aria-labelledby='simple-modal-title' aria-describedby='simple-modal-description'>
				{body}
			</MaterialUiModal>
		</div>
	);
}

export default Modal;
