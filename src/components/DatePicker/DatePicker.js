import 'date-fns';
import { useState } from 'react';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';

export default function MaterialUIPickers({ label, onChange, value, error }) {
	return (
		<MuiPickersUtilsProvider utils={DateFnsUtils}>
			<KeyboardDatePicker
				disableToolbar
				variant='inline'
				format='yyyy/MM/dd'
				margin='normal'
				id='date-picker-inline'
				label={label}
				value={value}
				onChange={onChange}
				error={!!error}
				helperText={error ? error.message : null}
				KeyboardButtonProps={{
					'aria-label': 'change date',
				}}
			/>
		</MuiPickersUtilsProvider>
	);
}
