import React from 'react';
import { getDay } from '../../helpers/date-formater.js';
import './Date.css';

function Date({ date }) {
	return (
        <div className='date-container'>
            <p>{getDay(date.date)}</p>
            <p>{date.date.split('-')[2]}</p>
        </div>
    )
}

export default Date;
