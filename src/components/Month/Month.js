import React from 'react';
import Date from '../Date/Date';
import { findStatusLength } from '../../helpers/status-length.js';
import './Month.css';

function Month({ month, dates }) {

	console.log(dates.map(date => date.status).lastIndexOf('blocked'));
	return (
		<div className='month'>
			<div className='month-header'>{month} 2021</div>
			<div className='month-dates'>
				{dates.map((date) => (
					<Date date={date} />
				))}
			</div>
			<div className='status-bar'>
				{dates.map((date, index) => (
					<div className={`status-column ${date.status}`}>
						{(index === 0 || (dates[index - 1] && date.status !== dates[index - 1].status)) && <div className={`status-text-${date.status}`} style={{ width: `calc(${findStatusLength(dates.slice(index), date.status)} * 60px)`}}>{date.status}</div>}
					</div>
				))}
			</div>
		</div>
	);
}

export default Month;
