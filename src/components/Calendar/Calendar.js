import React, { useRef } from 'react';
import { groupBy } from '../../helpers/group.js';
import Month from '../Month/Month.js';
import './Calendar.css';

function Calendar({ calendar }) {
	const calendarRef = useRef({});
	const groupedCalendar = groupBy(calendar, (date) => date.date.split('-')[1]);

	const scroll = (offset) => {
		calendarRef.current.scrollLeft += offset;
	};

	return (
		<div className='calendar'>
			<div className='main'>
				<div className='scroll-controls' onClick={() => scroll(-200)}>
					{'<'}
				</div>
				<div className='months-container' ref={calendarRef}>
					{[...groupedCalendar.entries()].map(([month, dates]) => (
						<>
							<Month month={month} dates={dates} />
						</>
					))}
				</div>
				<div className='scroll-controls' onClick={() => scroll(200)}>
					{'>'}
				</div>
			</div>
		</div>
	);
}

export default Calendar;
