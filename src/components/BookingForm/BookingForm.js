import { TextField, Button } from '@material-ui/core';
import React, { useRef, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import DatePicker from '../DatePicker/DatePicker';
import { format } from 'date-fns';
import { bookDates } from '../../helpers/bookDates.js';
import Alert from '../Alert/Alert';

function BookingForm({ calendar, setCalendar, setOpenModal }) {
	const [alertSucces, setAlertSuccess] = useState(false);
	const [alertError, setAlertError] = useState(false);
	const { handleSubmit, control, watch } = useForm();
	const checkinRef = useRef(null);
	checkinRef.current = watch('checkIn', new Date());

	const handleBooking = (data, calendar, setCalendar) => {
		const checkIn = format(data.checkIn, 'yyyy-MM-dd');
		const checkOut = format(data.checkOut, 'yyyy-MM-dd');

		const bookingResult = bookDates(checkIn, checkOut, calendar, setCalendar);

		if (bookingResult) {
			setCalendar(bookingResult);
			setAlertSuccess(true);

            setTimeout(() => {
                setOpenModal(false);
            }, 2000)
			
		} else {
			setAlertError(true);

            setTimeout(() => {
                setAlertError(false);
            }, 3000)
		}
	};

	return (
		<>
			<form className='login-form' onSubmit={(e) => e.preventDefault()}>
				<Controller
					name='checkIn'
					control={control}
					defaultValue={new Date()}
					rules={{
						required: 'Choose a check-in date',
					}}
					render={({ field: { onChange, value }, fieldState: { error } }) => (
						<DatePicker label='Check-in' ref={checkinRef} onChange={onChange} value={value} error={error} />
					)}
				/>
				<Controller
					name='checkOut'
					control={control}
					defaultValue={new Date()}
					rules={{
						validate: (value) => value?.getTime() > checkinRef.current?.getTime() || 'Pick a check-out date greater than the check-in date',
					}}
					render={({ field: { onChange, value }, fieldState: { error } }) => (
						<DatePicker label='Check-out' onChange={onChange} value={value} error={error} />
					)}
				/>
				<Controller
					name='name'
					control={control}
					defaultValue=''
					rules={{
						required: 'Name is required',
						minLength: {
							value: 3,
							message: 'Name must be at least 3 characters',
						},
						maxLength: {
							value: 100,
							message: 'Name must be no more than 100 characters',
						},
					}}
					render={({ field: { onChange, value }, fieldState: { error } }) => (
						<TextField
							fullWidth={true}
							color='primary'
							label='Names'
							variant='filled'
							value={value}
							onChange={onChange}
							error={!!error}
							helperText={error ? error.message : null}
							type='text'
						/>
					)}
				/>
				<Button
					fullWidth={true}
					type='submit'
					variant='contained'
					color='primary'
					onClick={handleSubmit((formData) => handleBooking(formData, calendar, setCalendar))}
				>
					Book Now
				</Button>
			</form>
			{alertSucces && <Alert type='success' title='Booking successful' text='Your booking was completed successfuly' />}
			{alertError && <Alert type='error' title='Booking not successful' text='Dates unavailable' />}
		</>
	);
}

export default BookingForm;
