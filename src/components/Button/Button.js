import './Button.css';

function Button({ text, onClick }) {
	return (
		<div className='button-wrapper'>
			<button className='book-btn' onClick={onClick}>{text}</button>
		</div>
	);
}

export default Button;
