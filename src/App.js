import calendarDates from './data/calendar.js';
import { useState } from 'react';
import './App.css';
import Calendar from './components/Calendar/Calendar.js';
import Modal from './components/Modal/Modal.js';
import { Button } from '@material-ui/core';

function App() {
	const [calendar, setCalendar] = useState(calendarDates);
	const [openModal, setOpenModal] = useState(false);

	return (
		<div className='content'>
			<Calendar calendar={calendar} />

			<div className='btn-container'>
				<Button fullWidth={false} type='button' variant='contained' color='primary' onClick={() => setOpenModal(true)}>
					Book Dates
				</Button>
			</div>

			<Modal openModal={openModal} setOpenModal={setOpenModal} calendar={calendar} setCalendar={setCalendar} />
		</div>
	);
}

export default App;
