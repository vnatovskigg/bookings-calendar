export const bookDates = (checkIn, checkOut, calendar, setCalendar) => {
    console.log(calendar);
    const first = calendar.map(e => e.date).indexOf(checkIn);
    const last = calendar.map(e => e.date).indexOf(checkOut);

    const bookingRange = calendar.slice(first, last + 1);
    console.log(bookingRange);

    const isBookable = bookingRange.every(e => e.status === 'bookable');

    if (!isBookable) return isBookable;
    else {
        for (let i = first; i <= last; i++) {
            calendar[i].status = 'booked';
        }
        
        return calendar;
    }
}