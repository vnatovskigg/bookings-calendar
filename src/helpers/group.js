export const groupBy = (list, keyGetter) => {
	const map = new Map();

	console.log(list);
	list.forEach((item) => {
		let key = keyGetter(item);

		switch (key) {
			case '01':
				key = 'Jan';
				break;
			case '02':
				key = 'Feb';
				break;
			case '03':
				key = 'Mar';
				break;
			case '04':
				key = 'Apr';
				break;
			case '05':
				key = 'May';
				break;
			case '06':
				key = 'Jun';
				break;
			case '07':
				key = 'Jul';
				break;
			case '08':
				key = 'Aug';
				break;
			case '09':
				key = 'Sep';
				break;
			case '10':
				key = 'Oct';
				break;
			case '11':
				key = 'Nov';
				break;
			case '12':
				key = 'Dec';
				break;
			default:
				return;
		}

		const collection = map.get(key);
		if (!collection) {
			map.set(key, [item]);
		} else {
			collection.push(item);
		}
	});
	return map;
};
